import fs from 'fs';
import * as configConstants from '../config/ConfigConstants';

class FSHelper {
    moveFile(oldFolder, newFolder, oldFile, newFile) {
        this.createFolder(newFolder);

        fs.rename(
            `${oldFolder}/${oldFile}`,
            `${newFolder}/${newFile}`,
            (err) => {
                if (err) throw err;
            },
        );
    }

    createFolder(folder) {
        if (!fs.existsSync(folder)) {
            fs.mkdirSync(folder);
        }
    }

    writeFile(path, file, data) {
        this.createFolder(path);
        fs.appendFileSync(`${path}/${file}`, data);
    }

    writeJsonFile(fileName, data) {
        this.createFolder(configConstants.JSON_DOWNLOAD_PATH);
        fs.writeFileSync(
            `${configConstants.JSON_DOWNLOAD_PATH}/${fileName}.json`,
            JSON.stringify(data, null, 2),
        );
    }

    readJsonFile(fileName) {
        return JSON.parse(
            fs.readFileSync(
                `${configConstants.JSON_BASE_FILES_PATH}/${fileName}.json`,
            ),
        );
    }

    async waitFileExists(file) {
        await browser.waitUntil(
            () => {
                return fs.existsSync(file);
            },
            {
                timeout: 20000,
                timeoutMsg: `Não foi encontrado arquivo no path: ${file}`,
            },
        );
    }
}

export const fsHelper = new FSHelper();
