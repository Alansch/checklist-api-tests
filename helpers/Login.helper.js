import nconf from '../config/EnvironmentVariables';
import supertest from 'supertest';
import logins from '../config/Logins.json';

const autorizacoes = {};
let url = `https://api-${nconf.get('APP_BASE_URL')}/mobile/`;

class LoginHelper {

    async getClient(usuario) {
        const hook = (method = 'post') => (args) =>
            supertest(url)
                [method](args)
                .set('authorization', token);

        const request = {
            post: hook('post'),
            get: hook('get'),
            put: hook('put'),
            patch: hook('patch'),
            delete: hook('delete'),
        };

        const user = logins[usuario];
        const token = await this.getToken(user);
        return request;
    }

    async getToken(user) {
        if (!(user.email in autorizacoes)) {
            const urlLogin = 'login?';
            const params = {
                email: user.email,
                password: user.senha,
            };

            const response = await supertest(url)
                .post(urlLogin)
                .send(params);
            autorizacoes[user.email] = {
                access_token: response.body.data.token,
            };

        }

        return `Bearer ${autorizacoes[user.email].access_token}`;
    }
}

export const loginHelper = new LoginHelper();
