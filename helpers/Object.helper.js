class ObjectHelper {
    removerChaves(obj, chaves) {
        Object.keys(obj).forEach((chave) => {
            if (chaves.some((k) => k === chave)) {
                delete obj[chave];
            } else if (this.isObject(obj[chave]) || this.isArray(obj[chave])) {
                this.removerChaves(obj[chave], chaves);
            }
        });

        return obj;
    }

    isObject(object) {
        return {}.toString.apply(object) === '[object Object]';
    }

    isArray(array) {
        return array instanceof Array;
    }

    objectIsEmpty(obj) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) return false;
        }
        return true;
    }

    getIndexOfColumns(columnsList, column) {
        const index = columnsList.indexOf(column);

        if (index >= 0) return index;

        throw new Error(
            `A coluna "${column}" não existe na tela. Colunas disponíveis na tela: "${columnsList}"`,
        );
    }
}

export const objectHelper = new ObjectHelper();
