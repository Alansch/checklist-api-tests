import debug from 'debug';

const descricaoLogs = 'TEST:LOGS:';
const nivelError = debug(`${descricaoLogs}ERROR`);
const nivelWarn = debug(`${descricaoLogs}WARN`);
const nivelInfo = debug(`${descricaoLogs}INFO`);
const nivelDebug = debug(`${descricaoLogs}DEBUG`);
const nivelTrace = debug(`${descricaoLogs}TRACE`);

class Logger {
    error(mensagem) {
        nivelError(mensagem);
    }

    warn(mensagem) {
        nivelWarn(mensagem);
    }

    info(mensagem) {
        nivelInfo(mensagem);
    }

    debug(mensagem) {
        nivelDebug(mensagem);
    }

    trace(mensagem) {
        nivelTrace(mensagem);
    }
}

export const logger = new Logger();
