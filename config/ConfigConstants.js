export const TEST_REPORT_PATH = './test-report';
export const TEST_REPORT_DOWNLOADS_PATH = `${TEST_REPORT_PATH}/Downloads`;
export const JSON_DOWNLOAD_PATH = `${TEST_REPORT_DOWNLOADS_PATH}/json`;
export const BASE_FILES_PATH = './base-files';
export const JSON_BASE_FILES_PATH = `${BASE_FILES_PATH}/json`;
