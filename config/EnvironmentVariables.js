const nconf = require('nconf');

nconf.argv().env().file({
    file: 'config.json',
    dir: './..',
    search: true,
});

const checkEmptyArg = (arg) => {
    const environment = nconf.get(arg);
    if (environment == null || environment === '') {
        throw new Error(
            `Variável "${arg}" está vazia ou não existe! Verifique o export!`,
        );
    }
};

const checkArgs = () => {
    checkEmptyArg('APP_BASE_URL');
    checkEmptyArg('SELENIUM_HOST');
    checkEmptyArg('E2E_TESTS_DB_HOST');
    checkEmptyArg('E2E_TESTS_DB_NAME');
    checkEmptyArg('E2E_TESTS_DB_USER');
    checkEmptyArg('E2E_TESTS_DB_PASSWORD');
    checkEmptyArg('MAILHOG_BASE_URL');
};

checkArgs();

module.exports = nconf;
