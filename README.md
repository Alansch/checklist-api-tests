# Testes para Api´s da Checklist Fácil

## Pré-requisitos
- node >= 12
- yarn >= 1.17

## Setup
Instalar as dependências:

```bash
$ yarn install
```
## Para executar os testes:
```bash
$ yarn init:tests
```

## Relatório de execução
Executar o comando para gerar o relatório:

```bash
$ allure generate
```

Executar o comando para abrir o relatório no navegador:

```bash
$ allure serve
```

## Lint do código

```bash
$ yarn code:format
```
