import { loginHelper } from '../../helpers/Login.helper';
import { Severity } from 'jest-allure/dist/Reporter';
import { fsHelper } from '../../helpers/FileSystem.helper';
import { objectHelper } from '../../helpers/Object.helper';

const url = 'user-info';

describe('user-info', () => {
    it('/GET USER: Admin 23', async () => {
        let client = await loginHelper.getClient('Admin 23');
        reporter.severity(Severity.Critical);
        const response = await client.get(url).expect(200);

        objectHelper.removerChaves(response.body, ['logo']);
        const reposeBase = fsHelper.readJsonFile('user_info_admin23');

        expect(response.body).toStrictEqual(reposeBase);
    });

    it('/GET USER: Usuário A', async () => {
        let client = await loginHelper.getClient('Usuario A');
        reporter.severity(Severity.Critical);
        const response = await client.get(url).expect(200);

        objectHelper.removerChaves(response.body, ['logo']);
        const reposeBase = fsHelper.readJsonFile('user_info_usuarioA');

        expect(response.body).toStrictEqual(reposeBase);
    });

    it('/GET USER: Usuário B', async () => {
        let client = await loginHelper.getClient('Usuario B');
        reporter.severity(Severity.Critical);
        const response = await client.get(url).expect(200);

        objectHelper.removerChaves(response.body, ['logo']);
        const reposeBase = fsHelper.readJsonFile('user_info_usuarioB');

        expect(response.body).toStrictEqual(reposeBase);
    });

});
